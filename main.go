package main

import (
	"fmt"

	"github.com/tredoe/osutil/user/crypt/sha512_crypt"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	salt = kingpin.Flag("salt", "Salt to use for hash").String()
	cost = kingpin.Flag("cost", "Number of rounds to hash").Default("5000").PlaceHolder("5000").Int()
	pass = kingpin.Arg("pass", "Password to hash").Required().String()
)

func main() {

	kingpin.Version("0.0.1")
	kingpin.Parse()

	c := sha512_crypt.New()
	var hash string

	if *salt == "" {
		s512 := sha512_crypt.GetSalt()
		gs := s512.GenerateWRounds(8, *cost)
		hash, _ = c.Generate([]byte(*pass), gs)
	} else {
		s := fmt.Sprintf("$6$rounds=%d$%s", *cost, *salt)

		hash, _ = c.Generate([]byte(*pass), []byte(s))
	}

	fmt.Println(hash)
}
